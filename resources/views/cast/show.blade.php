@extends('master')
@section('title')
    Data Cast {{$dataCast->id}}
@endsection

@section('content')
<table class="table">
    <thead class="thead-light">
      <tr>
        
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
      </tr>
    </thead>
    <tbody>
       
            <tr>
                
                <td>{{$dataCast->nama}}</td>
                <td>{{$dataCast->umur}}</td>
                <td>{{$dataCast->bio}}</td>
                
            </tr>                     
    </tbody>
</table>
@endsection